package com.mirrors_android_socket_client;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.Iterator;
import java.util.Set;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Environment;
import android.util.Log;

public class ClientTextBytes
{
	File file;
	String ipaddress = "10.0.0.10";
	int portNum = 9090;
	SocketChannel clientChannel;
	Selector selector;
	ByteBuffer readBuffer;
//	int bufferSize = AudioTrack.getMinBufferSize (
//			44100, 
//			AudioFormat.CHANNEL_OUT_STEREO, 
//			AudioFormat.ENCODING_PCM_16BIT
//			);
//	AudioTrack audioTrack = new AudioTrack(
//			AudioManager.STREAM_MUSIC, 
//			44100, 
//			AudioFormat.CHANNEL_OUT_STEREO, 
//			AudioFormat.ENCODING_PCM_16BIT, 
//			bufferSize, 
//			AudioTrack.MODE_STREAM
//			);
//	byte[] audioTrackArray = new byte[bufferSize];
//	short[] audioShortData;
	AudioSemaphoreClass audioDataQueue;
	
	public ClientTextBytes(AudioSemaphoreClass queue) 
	{
		audioDataQueue = queue;
	}

	String ReceiveClientFile() throws IOException
	{
		SocketChannel mySocket = null;
		try 
		{
			mySocket = SocketChannel.open();
			int bytesRead;
			ByteBuffer bb = ByteBuffer.allocate(1024);
			// non blocking
			mySocket.configureBlocking(false);
			Selector selector = Selector.open();
			mySocket.register(selector, SelectionKey.OP_CONNECT);
			mySocket.connect(new InetSocketAddress(ipaddress, portNum));
			//boolean isIPAddressReceived = false;
			//String ipportVals;
			int i = 0;
			while (i != -1) 
			{
				selector.select();
				Set keys = selector.selectedKeys();
				Iterator it = keys.iterator();
				while (it.hasNext())
				{
					SelectionKey key = (SelectionKey) it.next();
					SocketChannel myChannel = (SocketChannel) key.channel();
					it.remove();
					if (key.isValid() && key.isConnectable())
					{
						if (myChannel.isConnectionPending()) 
						{
							myChannel.finishConnect();
							mySocket.register(selector, SelectionKey.OP_READ);
						}
					}
					else if (key.isValid() && key.isReadable()) 
					{
						bb.clear();
						bytesRead = myChannel.read(bb);
						if(bytesRead == -1)
						{
							key.cancel();
							i = -1;
							break;
						}
						bb.flip();
						byte[] array = new byte[bb.remaining()];
						bb.get(array);
						audioDataQueue.put(array, array.length);
						bb.clear();
//						if(!isIPAddressReceived)
//						{
//							ipportVals  = new String(array, 0, array.length);
//							if(ipportVals.contains(Constants.ipaddressString))
//							{
//								String[] ipportsplit = ipportVals.split("#");
//								ipaddress = ipportsplit[1];
//								portNum = Integer.parseInt(ipportsplit[3]);
//							} 
//							isIPAddressReceived = true;
//						}
						
						//bufferedWriter.write(array);
						
//						audioShortData = new short[array.length/2];
//						bb.order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(audioShortData);
//						//audioTrack.write(array, 0, array.length);
//						audioTrack.write(audioShortData, 0, audioShortData.length);
//						if(audioTrackCount * 512 == bufferSize)
//						{
//							audioTrack.flush();
//							audioTrackCount = 1;
//						}
//						else
//						{
//							audioTrackCount ++;
//						}
					}
				}
			}

		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			//bufferedWriter.close();
			mySocket.close();
		}
		return "DONE";
	}
}
