package com.mirrors_android_socket_client;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.example.mirrors_android_socket_client.R;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.widget.TextView;

public class SocketChannelClientActivity extends Activity {
	TextView displayResult ;
	File file;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		displayResult = (TextView) findViewById(R.id.displayResult);
		//DeleteFilesFromMirrors();
		String filePath = Environment.getExternalStorageDirectory() + "/mirrors/mirrors_client";
		file = new File(filePath + System.currentTimeMillis());
		AudioSemaphoreClass pc = new AudioSemaphoreClass();
		if(Build.VERSION.SDK_INT >= 11)
		{
			new PlayAudioTrack().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, pc, null, null);	
			new RetrieveSocketClient().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, pc, null, null);
		}
		else
		{
			new PlayAudioTrack().execute(pc, null, null);
			new RetrieveSocketClient().execute(pc, null, null);
		}
	}

	private void DisplayText(String result)
	{
		displayResult.setText(result);
	}

	private void DeleteFilesFromMirrors()
	{
		String sdcard = Environment.getExternalStorageDirectory() + "/mirrors";
		File fileList = new File( sdcard );
		if (fileList != null)
		{
			File[] files = fileList.listFiles();
			for (File file : files)
			{
				file.delete();
			}
		}
	}


	class PlayAudioTrack extends AsyncTask<AudioSemaphoreClass, Void, String>
	{
		String result;
		BufferedOutputStream bufferedWriter;
		int bufferSize = AudioTrack.getMinBufferSize (
				44100, 
				AudioFormat.CHANNEL_OUT_STEREO, 
				AudioFormat.ENCODING_PCM_16BIT
				);
		AudioTrack audioTrack = new AudioTrack(
				AudioManager.STREAM_MUSIC, 
				44100, 
				AudioFormat.CHANNEL_OUT_STEREO, 
				AudioFormat.ENCODING_PCM_16BIT, 
				bufferSize, 
				AudioTrack.MODE_STREAM
				);
		protected String doInBackground(AudioSemaphoreClass... pcs)
		{
			WriteAudioTrackThread writetrack = new WriteAudioTrackThread(pcs[0]);
			try
			{
				bufferedWriter = new BufferedOutputStream(new FileOutputStream(file));
				result = writetrack.PlayAudioTrack(audioTrack, bufferSize, bufferedWriter);
			} catch (IOException e) 
			{
				e.printStackTrace();
			}
			return result;
		}
		protected void onPostExecute(String result) 
		{
			audioTrack.stop();
			audioTrack.release();
			try
			{
				bufferedWriter.close();
			} catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}

	class RetrieveSocketClient extends AsyncTask<AudioSemaphoreClass, Void, String>
	{
		String result;
		protected String doInBackground(AudioSemaphoreClass... pcs)
		{

			ClientTextBytes ctxbytes = new ClientTextBytes(pcs[0]);
			try {
				result = ctxbytes.ReceiveClientFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			//			ClientStaticAudioBytes audioBytes = new ClientStaticAudioBytes();
			//			try {
			//				audioBytes.ReceiveAudioBytes();
			//			} catch (IOException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			}
			return result;
		}
		//		
		//		protected String doInBackground(Void... urls)
		//		{
		//			Socket socket = null;
		//			String serverAddress = "192.168.0.6";
		//			BufferedInputStream in = null;
		//			//BufferedOutputStream out;
		//			String i = "" ;
		//			byte[] buffer = null;
		//			try 
		//			{
		//				socket = new Socket(serverAddress, 9090);
		//				in= new  BufferedInputStream( socket.getInputStream());
		//				//out = new BufferedOutputStream(socket.getOutputStream());
		//				int length = 1024;
		//				int j =0;
		//				while(j != -1)
		//				{
		//				buffer = new byte[length];
		//				j = in.read(buffer, 0, length);
		//				Thread.sleep(100);
		//				Log.d("abcdef",  String.valueOf(j)+"###################"+ String.valueOf(buffer[1023]));
		//				}
		//			}
		//			catch (UnknownHostException e)
		//			{
		//				e.printStackTrace();
		//			} catch (IOException e) 
		//			{
		//				e.printStackTrace();
		//			} catch (InterruptedException e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
		//			finally {
		//				try {
		//					in.close();
		//					socket.close();
		//				} catch (IOException e) {
		//					// TODO Auto-generated catch block
		//					e.printStackTrace();
		//				}
		//				
		//			} 
		//			return  "a";
		//		}

		protected void onPostExecute(String result) 
		{
			DisplayText(result);

		}
	}

}
