package com.mirrors_android_socket_client;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import android.media.AudioTrack;

public class WriteAudioTrackThread extends Thread
{
	AudioSemaphoreClass pc;
	WriteAudioTrackThread(AudioSemaphoreClass pc0)
	{
		pc = pc0;
	}
	public String PlayAudioTrack(AudioTrack audioTrack, int bufferSize, BufferedOutputStream bufferedWriter) throws IOException
	{
		int audioTrackCount = 1;
		//byte[] audioTrackArray = new byte[bufferSize];
		short[] audioShortData;
		ByteBuffer bb = ByteBuffer.allocate(1024);
		byte[] array;
		audioTrack.play();
		while(true)
		{
			bb.clear();
			bb.put(pc.get());
			bb.flip();
			audioShortData = new short[bb.remaining()/2];
			bb.order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(audioShortData);
			//audioTrack.write(array, 0, array.length);
			audioTrack.write(audioShortData, 0, audioShortData.length);
			if(audioTrackCount * 512 == bufferSize)
			{
				audioTrack.flush();
				audioTrackCount = 1;
			}
			else
			{
				audioTrackCount ++;
				
			}
			array = new byte[bb.remaining()];
			bb.get(array);
			bufferedWriter.write(array);
		}
	}

}
