package com.mirrors_android_socket_client;

public class AudioSemaphoreClass 
{
	int BUFFER_SIZE = 1024;
	int byteArraySize = 0;
	//ByteBuffer item = ByteBuffer.allocate(1024);
	boolean isAvailable = false;
	byte[] bytesData;
	
	public synchronized void put(byte[] value, int byteCount)
	{
		while(isAvailable == true)
		{
			try 
			{
				wait();
			} catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
//		item.clear();
//		item.wrap(value, 0, byteCount);
		bytesData = new byte[byteCount];
		bytesData = value;
		isAvailable = true;
		notifyAll();
	}
	
	public synchronized byte[] get()
	{
		//byte[] array = new byte[byteArraySize];
		while(isAvailable == false)
		{
			try
			{
				wait();
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		isAvailable = false;
		notifyAll();
		//item.flip();
		return bytesData;
	}
}
